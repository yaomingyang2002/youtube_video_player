# About this Application and the project:
This is a complete React Application for searching YouTube videos.

This project shows the development, step-by-step working flow, of a React-Redux (JSX, ES6) App based on Youtube Search API. This App was developed with Atom on Mac OS, incorporated with Git and Git flow and used a react-redux project starter (https://github.com/StephenGrider/ReduxSimpleStarter.git).

# Getting Started / installation

There are two methods for getting started with this repo.

1. Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone this repo
> cd youtube_video_player
> npm install
> npm start
> http://localhost:8080/webpack-dev-server/
> search youtube video and play
> clt+c to stop server
```
2. download the .zip file.  Extract the contents of the zip file, then open your terminal, change to the project directory, and:

```
> npm install
> npm start
> http://localhost:8080/webpack-dev-server/
> search youtube video and play
> clt+c to stop server
```

# Learning or refreshing React.js knowledge:

Read the code following step-by-step working flow with each commit.
